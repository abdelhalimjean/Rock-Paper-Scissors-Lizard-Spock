const HANDS=document.getElementsByTagName('li');
const OPTIONS=['rock','paper','scissors','lizard','spock'];
const FEEDBACK_MSG_BOX=document.getElementById('feedback-msg');

const messages={
	'scVSp':'Scissors cuts Paper',
	'pVSr':'Paper covers Rock',
	'rVSl':'Rock crushes Lizard',
	'lVSsp':'Lizard poisons Spock',
	'spVSSc':'Spock smashes Scissors',
	'scVSl':'Scissors decapitates Lizard',
	'lVSp':'Lizard eats Paper',
	'pVSsp':'Paper disproves Spock',
	'spVSr':'Spock vaporizes Rock',
	'rVSsc':'Rock crushes Scissors',
};

const USERS_HAND_INDICATOR_ID='users-hand';
const COMPUTERS_HAND_INDICATOR_ID='computers-hand';

var score=JSON.parse(localStorage.getItem('score'))||{
	'wins':0,'losses':0,'draws':0,'winStreak':0,'bestStreak':0
};

//Declaring global variables that will be used in different functions
var result;
var UsersHand_LI;
var ComputersHand_LI;

//Adding event handles to all the LI elements (Hands)
for (let i = 0; i < HANDS.length; i++) {
	HANDS[i].addEventListener('click',function(){
		main(HANDS[i].id);
	},false);
}

//On page laod we want to update the scoring board with the values saved in the local storage
updateScoreBoard();


/* ----------- Functions -----------  */
function main(UsersHand){
	cleanUpUI();

	//Computer playing
	ComputersHand=OPTIONS[Math.floor(Math.random()*OPTIONS.length)];
	result=clash(UsersHand,ComputersHand);

	//Adding the user's hand indicator
	UsersHand_LI=document.getElementById(UsersHand);
	UsersHand_LI.innerHTML+='<i id="'+USERS_HAND_INDICATOR_ID+'" class="fas fa-user users_hand_indicator"></i>';

	//Adding the computer's hand indicator
	ComputersHandLI=document.getElementById(ComputersHand);
	ComputersHandLI.innerHTML+='<i id="'+COMPUTERS_HAND_INDICATOR_ID+'"  class="fas fa-laptop computers_hand_indicator"></i>';

	//Updating the score
	if(result[0]==0){
		score.draws++;
	}else if(result[0]==1){
		score.wins++;
		score.winStreak++;
	}else if(result[0]==-1){
		score.losses++;
		score.winStreak=0;
	}

	saveScore(result[0]);

	updateUI(UsersHand_LI);
}


function updateUI(UsersHand_LI){
	var result_box='';
	switch (result[0]) {
		case -1:
			result_box='lose-msg';
			UsersHand_LI.classList.add('lose');
			break;
		case 0:
			result_box='draw-msg';
			UsersHand_LI.classList.add('draw');
			break;
		case 1:
			result_box='win-msg';
			UsersHand_LI.classList.add('win');
		break;
	}
	document.getElementById(result_box).style.display="inline-block";
	document.getElementById('explanation-msg').innerHTML=result[1];
	updateScoreBoard();
}

function updateScoreBoard(){
	document.getElementById('wins').innerHTML=score.wins;
	document.getElementById('losses').innerHTML=score.losses;
	document.getElementById('win-streak').children[0].innerHTML=score.winStreak;
	document.getElementById('win-streak').children[1].innerHTML=score.bestStreak;
}

function saveScore(){
	if(score.winStreak>score.bestStreak){
		score.bestStreak=score.winStreak;
	}
	localStorage.setItem('score',JSON.stringify(score));
}

function clash(UsersHand,ComputersHand) {
	switch (UsersHand) {
		case 'rock':
			switch (ComputersHand) {
				case 'paper':
					return [-1,messages.pVSr];
				case 'spock':
					return [-1,messages.spVSr];

				case 'scissors':
				return [1,messages.rVSsc];
				case 'lizard':
					return [1,messages.rVSl];

				default:
					return [0,''];
			}
		case 'paper':
			switch (ComputersHand) {
				case 'scissors':
					return [-1,messages.scVSp];
				case 'lizard':
					return [-1,messages.lVSp];

				case 'rock':
					return [1,messages.pVSr];
				case 'spock':
					return [1,messages.pVSsp];

				default:
					return [0,''];
			}
		case 'scissors':
			switch (ComputersHand) {
				case 'rock':
					return [-1,messages.rVSsc];
				case 'spock':
					return [-1,messages.spVSSc];

				case 'paper':
					return [1,messages.scVSp];
				case 'lizard':
					return [1,messages.scVSl];
				default:
					return [0,''];
			}
		case 'lizard':
			switch (ComputersHand) {
				case 'rock':
					return [-1,messages.rVSl];
				case 'scissors':
					return [-1,messages.scVSl];

				case 'paper':
					return [1,messages.lVSp];
				case 'spock':
					return [1,messages.lVSsp];

				default:
					return [0,''];
			}
		case 'spock':
			switch (ComputersHand) {
				case 'lizard':
					return [-1,messages.lVSsp];
				case 'paper':
					return [-1,messages.pVSsp];

				case 'scissors':
					return [1,messages.spVSSc];
				case 'rock':
					return [1,messages.spVSr];

				default:
					return [0,''];
			}
	}
}

function playWithoutMessages(UsersHand,ComputersHand) {
	switch (UsersHand) {
		case 'rock':
			switch (ComputersHand) {
				case 'paper','spock':
					return -1;
				case 'scissors','lizard':
					return 1;
				default:
					return 0;
			}
		case 'paper':
			switch (ComputersHand) {
				case 'scissors','lizard':
					return -1;
				case 'rock','spock':
					return 1;
				default:
					return 0;
			}
		case 'scissors':
			switch (ComputersHand) {
				case 'rock','spock':
					return -1;
				case 'paper','lizard':
					return 1;
				default:
					return 0;
			}
		case 'lizard':
			switch (ComputersHand) {
			case 'rock','scissors':
				return -1;
			case 'paper','spock':
				return 1;
			default:
				return 0;
			}
		case 'spock':
			switch (ComputersHand) {
				case 'lizard','paper':
					return -1;
				case 'scissors','rock':
					return 1;
				default:
					return 0;
			}
	}
}

function cleanUpUI(){//Removes certain classes and get everything back to normal
	/*
		V1 : Preventing the error (when trying to remove an item that doesn't exist) directly
			by checking if the element exists on the DOM before deleting it
			but we have 3 if statement that will be repeated every game
	*/
	if(document.contains(document.getElementById(USERS_HAND_INDICATOR_ID))){
		//Removing the users hand indicator
		document.getElementById(USERS_HAND_INDICATOR_ID).remove();
	}
	if(document.contains(document.getElementById(COMPUTERS_HAND_INDICATOR_ID))){
		//Removing the computerss hand indicator
		document.getElementById(COMPUTERS_HAND_INDICATOR_ID).remove();
	}
	if(document.contains(UsersHand_LI)){
		//Removing the class from the users pkayed hand
		UsersHand_LI.classList.remove('win','lose','draw');
	}
	for (let i = 0; i < FEEDBACK_MSG_BOX.children.length; i++) {
		FEEDBACK_MSG_BOX.children[i].style.display='none';
	}
}
function cleanUpUIV2(){//Removes certain classes and get everything back to normal
	/*
		V2 : Preventing the error (when trying to remove an item that doesn't exist)
			by checking the result variable which -according to the logic of the code-
			will be null on game start; meaning there are no elements, so if it's not null
			that means the elements exist so we can delete them.
			besides we'll have only 1 if statement that will be repeated every game,not really that much
			but i just thought it would be a good idea to have both solutions here, the first version is definitly more precise and accurate
	*/
	if(result!=null){
		//Removing the users hand indicator
		document.getElementById(USERS_HAND_INDICATOR_ID).remove();
		//Removing the computerss hand indicator
		document.getElementById(COMPUTERS_HAND_INDICATOR_ID).remove();
		//Removing the class from the users pkayed hand
		UsersHand_LI.classList.remove('win','lose','draw');
		for (let i = 0; i < FEEDBACK_MSG_BOX.children.length; i++) {
			FEEDBACK_MSG_BOX.children[i].style.display='none';
		}
	}
}
function cleanUpUIV3(){///Removes certain classes and get everything back to normal
	// V3 : Preventing the error (when trying to remove an item that doesn't exist) by using a try and catch
	try{
		//Removing the users hand indicator
		document.getElementById(USERS_HAND_INDICATOR_ID).remove();
		//Removing the computerss hand indicator
		document.getElementById(COMPUTERS_HAND_INDICATOR_ID).remove();
		//Removing the class from the users pkayed hand
		UsersHand_LI.classList.remove('win','lose','draw');
	}catch(err){
		// console.log(err);
	}
	for (let i = 0; i < FEEDBACK_MSG_BOX.children.length; i++) {
		FEEDBACK_MSG_BOX.children[i].style.display='none';
	}
}

function showHelp(){
	document.getElementById('toggleModal').classList.toggle('close');
	document.getElementById('toggleModal').classList.toggle('help');
	document.getElementById('toggleModal').classList.toggle('fa-info-circle');
	document.getElementById('toggleModal').classList.toggle('fa-times');
	document.getElementById('modal').classList.toggle('hidden');
}
